<?php

// this file is special 
function load_css (){
    wp_register_style('serost_mystylesheet', get_template_directory_uri() . '/assets/css/mystyle.css', array(), false, 'all');
    wp_enqueue_style('serost_mystylesheet');
}

add_action('wp_enqueue_scripts', 'load_css');